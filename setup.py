from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in rwb_wps/__init__.py
from rwb_wps import __version__ as version

setup(
	name="rwb_wps",
	version=version,
	description="RWB app for granting water use permit for water users",
	author="Africlouds Ltd",
	author_email="info@africlouds.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
