from frappe import _

def get_data():
	return [
		{
			"module_name": "Rwb Wps",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Rwb Wps")
		},
                {
                        "module_name": "Water Permit",
                        "color":"grey",
                        "icon": "octicon octicon-file-directory",
                        "type": "module",
                        "label": ("Water Permit")
                }
	]
